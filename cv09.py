def check_string(string):
    if len(string) <= 2:
        if string == "ab":
            return True
        return False
    return check_string(string[1:len(string)-1])

from turtle import Turtle
from random import randint
#turtle = Turtle()

def koch(n, size):
    if n == 0:
        turtle.forward(size)
    else:
        koch(n-1, size/3)
        turtle.left(60)
        koch(n-1, size/3)
        turtle.right(120)
        koch(n-1, size/3)
        turtle.left(60)
        koch(n-1, size/3)

def compare_min(a, b):
    if a <= b:
        return a
    else: return b

def compare_max(a, b):
    if a >= b:
        return a
    else: return b

def min_max(array):
    if len(array) == 2:
        if array[0] >= array[1]:
            return (array[1], array[0])
        else:
            return (array[0], array[1])
    if len(array) == 1:
        return (array[0], array[0])
    return (compare_min(min_max(array[0:int(len(array)/2)])[0], min_max(array[int(len(array)/2):len(array)])[0]), compare_max(min_max(array[0:int(len(array)/2)])[1], min_max(array[int(len(array)/2):len(array)])[1]))



def quicksort(alist):
    if len(alist) == 1:
        return alist
    pivot = randint(0, len(alist)
    return list(quicksort(alist[0:pivot-1] + alist[pivot] + quicksort(alist[pivot:len(alist)]))
        
    
