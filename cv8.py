def evaluate_postfix(postfix):
    stack = []
    operators = ["*", "-", "+", "/"]
    for i in postfix.split():
        if i in operators:
            a = stack.pop()
            b = stack.pop()
            if i == "*":
                stack.append(a*b)
            if i == "-":
                stack.append(b-a)
            if i == "+":
                stack.append(a+b)
            if i == "/":
                stack.append(b/a)
        else: stack.append(int(i))
    return stack


def morse2text(morse):

    morse_chars = {".-": "a", "-...": "b", "-.-.": "c", "-..": "d", ".": "e", "..-.": "f", "--.": "g", "....": "h", "..": "i", ".---": "j", "-.-": "k", ".-..": "l", "--": "m", "-.": "n", "---": "o", ".--.": "p", "--.-": "q", ".-.": "r", "...": "s", "-": "t", "..-": "u", "...-": "v", ".--": "w", "-..-": "x", "-.--": "y", "--..": "z"}
    text = ""
    for i in morse.split("|"):
        for j in i.split(" "):
            print(j)
            text += morse_chars.get(j, " ")
    return text
