#Autor: Martin Sedláček, 485091
#Prohlašuji, že celý zdrojový kód jsem zpracoval(a) zcela samostatně.
#Jsem si vědom(a), že  nepravdivost tohoto tvrzení může být důvodem
#k hodnocení F v předmětu IB111 a k disciplinárnímu řízení.

# Známé nedostatky: Žádné, ale plnou korektností si nejsem jistý.
# Styl: Snad v pořádku.

from random import randint

def print_board(board):
    """
    print board
    :param board: 2D list
    """
    print("    ", end="")
    for i in range(1,len(board[0])+1):print(i, end=" ")
    print()
    print("  +","-"*((len(board[0])*2)-1), "+")
    for row in range(len(board)):
        print(row+1, "| ", end="")
        for column in range(len(board[0])):
            print(board[row][column], end=" ")
        print("|")
    print("  +","-"*((len(board[0])*2)-1), "+")


def is_not_end(board):
    """
    true if there is not blocked field on the board
    :param board: 2D list
    :return: bool
    """
    for i in board:
        if "." in i:
            return True
    return False

def get_max_impact(board):
    greatest_impact = [0,0]
    for y in range(len(board)):
        for x in range(len(board[0])):
            if board[y][x] == ".":
                count = -1
                for i in range(y-1, y+2):
                    if 0 <= i < len(board):
                        for j in range(x-1, x+2):
                            if 0 <= j < len(board[0]):
                                if board[i][j] == "." :
                                    count += 1
                if count >= greatest_impact[1]:
                    greatest_impact = [(y, x), count]
    return greatest_impact

def num_of_free_spots(board):
    count = 0
    for y in range(len(board)):
        for x in range(len(board[0])):
            if board[y][x] == ".":
                count += 1
    return count

def get_first_avaible_pos(board):
    for i in range(0, len(board)):
        if "." in board[i]:
            return (i,board[i].index("."))

def get_random_input(board):
    possible_y = []
    for y in range(len(board)):
        if "." in board[y]:
            possible_y.append(y)
    y = possible_y[randint(0, len(possible_y)-1)]

    possible_x = []
    for x in range(len(board[0])):
        if "." == board[y][x]:
            possible_x.append(x)
    x = possible_x[randint(0, len(possible_x)-1)]

    return(y, x)

def get_player_input(board):
    y = get_players_number(board, "y")
    x = get_players_number(board, "x")
    if not is_not_occupied((y, x), board):
        print("Your selected coordinates are already occupied!!")
        return get_player_input(board)
    return(y, x)

def get_players_number(board, coordinate):
    if coordinate == "y":
        player_input = input("Enter first coordinate (row): ")
    if coordinate == "x":
        player_input = input("Enter second coordinate (column): ")
    if is_positive_number(player_input) and is_in_board_range(player_input, board, coordinate):
        return int(player_input)-1
    return get_players_number(board, coordinate)

def is_positive_number(value):
    try:
        num = int(value)
        if num > 0: return True
        else:
            print("You must enter valid number!!")
            return False
    except ValueError:
        print("You must enter valid number!!")
        return False

def is_in_board_range(value, board, coordinate):
    if coordinate == "x" and int(value) <= len(board[0]):
        return True
    if coordinate == "y" and int(value) <= len(board):
        return True
    print("Coordinate out of range!!")
    return False

def is_not_occupied(value, board):
    return board[value[0]][value[1]] == "."

def block_possitions(board, coords):
    for i in range(coords[0]-1, coords[0]+2):
        if 0 <= i < len(board):
            for j in range(coords[1]-1, coords[1]+2):
                if 0 <= j < len(board[0]):
                    if board[i][j] == "." :
                        board[i][j] = "#"
    return board

def human_player(board, char):
    """
    funkce umoznuje hrat BLOCK GAME lidskemu hraci
    Nejprve vypise hraci pole a nabidne hraci zadat souradnice jeho tahu.
    Kazdy tah zkontroluje zda je validni. Funkce nesm9 spadnout s chybou, pokud
    hrac chce hrat na pole, ktere je jiz blokovane, ktere je mimo hraci plan nebo
    zada misto souradnic nahodny sled znaku. Pri nevalidnim vstupu funkce popise
    problem a necha hrace hrat znovu.

    :param board: instance hraciho planu pred tahem
    :param char: identifikacni znak hrace
    :return: upravene hraci pole
    """
    coordinates = get_player_input(board)
    board = block_possitions(board, coordinates)
    board[coordinates[0]][coordinates[1]] = char
    return board

def ai_random(board, char):
    """
    strategie, volici dalsi tah nahodne

    :param board: instance hraciho planu pred tahem
    :param char: identifikacni znak hrace
    :return: upravene hraci pole
    """
    coordinates = get_random_input(board)
    board = block_possitions(board, coordinates)
    board[coordinates[0]][coordinates[1]] = char
    return board


def ai_first_available(board, char):
    """
    strategie, volici dalsi tah jako prvni volne pole pri pruchodu
    hraciho planu po radcich

    :param board: instance hraciho planu pred tahem
    :param char: identifikacni znak hrace
    :return: upravene hraci pole
    """
    coordinates = get_first_avaible_pos(board)
    board = block_possitions(board, coordinates)
    board[coordinates[0]][coordinates[1]] = char
    return board

def ai_max_impact(board, char):
    """
    tahne na souradnice, na kterych zablokuje co nejvice poli

    :param board: instance hraciho planu pred tahem
    :param char: identifikacni znak hrace
    :return: upravene hraci pole
    """
    coordinates = get_max_impact(board)[0]
    board = block_possitions(board, coordinates)
    board[coordinates[0]][coordinates[1]] = char
    return board

def ai_last_shot(board, char):
    """
    pokud je moze v tomto kole zvitezit, zvitezi, jinak hraje nahodne

    :param board: instance hraciho planu pred tahem
    :param char: identifikacni znak hrace
    :return: upravene hraci pole
    """
    if num_of_free_spots(board)-1 == get_max_impact(board)[1]:
        coordinates = get_max_impact(board)[0]
    else:
        coordinates = get_random_input(board)
    board = block_possitions(board, coordinates)
    board[coordinates[0]][coordinates[1]] = char
    return board



def block_game(length, height, output=True, player1=human_player, player2=human_player, chars = ["x", "o"]):
    """
    funkce spusti jednu instanci BLOCK GAME
    funkce prijima ruzne strategie hry, zakladni strategii je interakce s lidskym hracem
    hraci plan je instanci teto funkce, kazda strategie, pokud je zavolana, zmeni stav planu
    funkce tiskne informace o hre, pokud je nastaveny parametr output na True

    :param chars: charakteristické znaky hráčů
    :param length: delka hraciho planu, od 1 do 9
    :param height: vyska hraciho planu, od 1 do 9
    :param output: True, pokud se vypisuji informace o hre
    :param player1: strategie prvniho hrace
    :param player2: strategie druheho hrace
    :return: pocet odehranych kol

    example:
    cast vypisu hry 6 x 5
    ctvrte kolo, na tahu je hrac 'x'
    vypis noveho stavu hraciho planu na zacatku pateho kola:


    ...
    ROUND 4
    player 'x'
        1 2 3 4 5
      +-----------+
    1 | . # # # . |
    2 | # # o # . |
    3 | x # # # # |
    4 | # # . # o |
    5 | . . . # # |
    6 | . . . . . |
      +-----------+
    first coordinate: >? 6
    second coordinate: >? 1

    ROUND 5
    player 'o'
        1 2 3 4 5
      +-----------+
    1 | . # # # . |
    2 | # # o # . |
    3 | x # # # # |
    4 | # # . # o |
    5 | # # . # # |
    6 | x # . . . |
      +-----------+
    first coordinate: >? |
    ...

    """

    board = [['.' for y in range(height)]
                  for x in range(length)]

    game_round = 1
    while is_not_end(board):
        if output:
            print()
            print('ROUND {}'.format(game_round), end="   ")
            print("PLAYER", ((game_round + 1) % 2) + 1, "- [", chars[(game_round + 1) % 2], "]")
            print_board(board)

        if game_round % 2 == 1:
            board = player1(board, chars[0])
        else:
            board = player2(board, chars[1])
        game_round += 1

    if output:
        print()
        print('END OF GAME')
        print_board(board)
        print("PLAYER", (game_round % 2) + 1, "- [", chars[game_round % 2], "] WON!!")
    return game_round - 1


def block_game_statistic(player1, player2, repetitions=10):
    """
    Spocita statistiku, v jakem procentu pripadu vyhraje prvni strategie.
    Statistika se dela pres ruzne rozmery hraciho planu od 1 do 9 pro obe dimenze

    :param repetitions: počet opakování pro každou velikost hracího pole
    :param player1: funkce zkoumane strategie
    :param player2: funkce druhe strategie
    :return:

    example:
    >>> block_game_statistic(ai_first_available, ai_last_shot)
    Player 1 won in 44.81% matches

    """
    player1_wins = 0
    total_games = 0
    for length in range(1, 10):
        for height in range(length, 10):
            for i in range (repetitions):
                total_games += 1
                if block_game(length, height, output=False, player1=player1, player2=player2, chars = ["x", "o"]) % 2 == 0:
                    player1_wins += 1
    print("Player1 won in", round((player1_wins/total_games)*100, 2), "% matches.")


