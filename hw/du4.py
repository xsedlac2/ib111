# Autor: Martin Sedláček, 485091

# Prohlašuji, že celý zdrojový kód jsem zpracoval(a) zcela samostatně.
# Jsem si vědom(a), že  nepravdivost tohoto tvrzení může být
# důvodem k hodnocení F v předmětu IB111 a k disciplinárnímu řízení.

#SEBEHODNOCENÍ:
#ZNÁMÉ NEDOSTATKY: Vědomě není implementovaní metoda .get_total_size() u třídy Folder,
#kvůli překrývání s funkcionalitou metody .get_size() --> metoda .get_size() přebýrá její funkci a také plní svoji původní fci,
#a to v případě, kdy je složka prázdná. - bližší informace jsem psal v mailu, na který jsem ale nedostal odpověď

#ZNÁMÉ NEDOSTATKY: Příkaz print(fs.get_size("/")) vrací jinou hodnotu než je uvedena v zadaní, avšak domnívám se, že hodnotu vrací správnou.
#Při ručním sečtení všech velikostí mi vychází číslo stejné, jako vrací můj program. Tato hodnota je oproti hodnotě v zadání menší o 4096,
#tj. velikost jedné složky, takže se domnívám, že bylo poítáno s tím, že samotný FS má velikost, což mi nepříjde jako korektní implementace.

#STYL: Jednotlivé třídy mají velice podobné metody, což by bylo pěkné nejak sjednotit, avšak ani po hledání na internetu jsem nenašel
#vhodný způsob jak na to. Jde-li to vůbec.


class File:
    def __init__(self, name, size):
        self.name = name
        self.size = int(size)

    def get_name(self):
        return self.name

    def get_size(self, path=[]):
        return self.size

    def list_dir(self, path):
        print("ERROR: file cannot be listed")
        return None

    def check_path(self, path):
        return True

    def __str__(self):
        file_desc = "f - " + str(self.size) + " " + self.name
        return file_desc


class Folder:
    def __init__(self, name, size=4096):
        self.name = name
        self.size = int(size)
        self.content = {}

    def add_folder(self, name, size=4096):
        self.content[name] = (Folder(name, size))

    def add_file(self, name, size):
        self.content[name] = (File(name, size))

    def get_name(self):
        return self.name

    # def get_size(self):
    # return self.size

    def get_size(self, path=[]):
        total_size = 0
        if len(path) == 0:
            total_size += self.size
        for key in self.content.keys():
            if len(path) > 0:
                if path[0] == key:
                    total_size += self.content[key].get_size(path[1::])
                    break
            else:
                total_size += self.content[key].get_size()
        return total_size

    def print_structure(self, level=0):
        for item in self.content.values():
            print("|  " * level, "|--", item.get_name(), sep="")
            if type(item) == Folder:
                item.print_structure(level + 1)

    def check_path(self, path):
        if len(path) == 0:
            return True
        if path[0] in list(self.content.keys()):
            return self.content[path[0]].check_path(path[1::])
        else:
            return False

    def list_dir(self, path):
        for key in self.content.keys():
            if len(path) > 0:
                if path[0] == key:
                    return self.content[key].list_dir(path[1::])
            else:
                print(self.content[key].get_name())

    def __str__(self):
        folder_desc = "d - " + str(self.size) + " " + self.name
        return folder_desc


class FileSystem:
    def __init__(self, text_file):
        self.name = "FS"
        self.content = {}

        with open(text_file, "r") as file:

            for line in file.readlines():
                line = line.strip()

                if len(line) != 0 and line[0] == ".":
                    path = line.replace(".", "").replace(":", "").split("/")
                    del path[0]
                    folder = self
                    while len(path) > 0:
                        folder = folder.content[path.pop(0)]

                if len(line) != 0 and line[0] == "d":
                    size = line.split(" ")[1]
                    name = line.split(" ")[2]
                    folder.add_folder(name, size)

                if len(line) != 0 and line[0:2] == "-r":
                    size = line.split(" ")[1]
                    name = line.split(" ")[2]
                    folder.add_file(name, size)

    def add_folder(self, name, size=4096):
        self.content[name] = (Folder(name, size))

    def add_file(self, name, size):
        self.content[name] = (File(name, size))

    def get_size(self, path):
        path = path.split("/")[1::]
        if self.check_path(path):
            total_size = 0
            for key in self.content.keys():
                if len(path) > 0 and path[0] != "":
                    if path[0] == key:
                        total_size += self.content[key].get_size(path[1::])
                        break
                else:
                    total_size += self.content[key].get_size()
            return total_size
        else:
            print("ERROR: path is not valid")
            return None

    def print_structure(self, level=0):
        for item in self.content.values():
            print("|  " * level, "|--", item.get_name(), sep="")
            if type(item) == Folder:
                item.print_structure(level + 1)

    def check_path(self, path):
        if len(path) == 1 and path[0] == "":
            return True
        if len(path) > 0 and path[0] in list(self.content.keys()):
            return self.content[path[0]].check_path(path[1::])
        else:
            return False

    def list_dir(self, path):
        path = path.split("/")[1::]
        if self.check_path(path):
            for key in self.content.keys():
                if len(path) > 0 and path[0] != "":
                    if path[0] == key:
                        return self.content[key].list_dir(path[1::])
                else:
                    print(self.content[key].get_name())
        else:
            print("ERROR: path is not valid")
            return None


fs = FileSystem("du4.txt")
