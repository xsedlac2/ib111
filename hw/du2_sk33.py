#Autor: Martin Sedláček, 485091
#Prohlašuji, že celý zdrojový kód jsem zpracoval(a) zcela samostatně.
#Jsem si vědom(a), že  nepravdivost tohoto tvrzení může být
#důvodem k hodnocení F v předmětu IB111 a k disciplinárnímu řízení.

from random import randint, random
import math


# SEBEHODNOCENÍ:
# funkce game podle mého funguje správně a nemá žádné nedostatky
# problém vidím v použití zbytečně moc výpisů print() v podfunkci print_output()
def game(length, output = True, prob_six = 1/6):
    position = 0
    turn = 0
    play_field = ["#"]
    for x in range (length):
        play_field.append(".")
    while position != length:
        turn += 1
        current_throw = roll_dice(prob_six)
        if position + current_throw <= length and current_throw != 18:
            play_field[position] = "."
            position += current_throw
            play_field[position] = "#"
        if output == True:
            print_output(play_field, turn, current_throw)   
        if position == length: return turn

def cube_throw (prob_six):
    if randint(0,99) < prob_six*100:
        return 6
    else:
        return randint(1,5)

def roll_dice (prob_six, current_throw = 0):
    current_throw += cube_throw(prob_six)
    if current_throw % 6 == 0 and current_throw != 18:
        return roll_dice(prob_six, current_throw)
    return current_throw

def get_thrown_nums (sum_of_throws):
    numbers = ""
    while sum_of_throws//6 >0 and sum_of_throws != 6:
        sum_of_throws -= 6
        numbers += "6,"
    return numbers + str(sum_of_throws)
 
def print_output (play_field, turn, current_throw):
    print ("Turn:  ", turn, "  Throws:", get_thrown_nums(current_throw))
    for i in play_field:
        print (i, end="")
    print()
    for i in range(0, len(play_field)):
        print(i%10, end="")
        if i%10 == 9 and i != 0:
            print()
            print(" "*int(10*((i+1)/10)), end="")
    print()

# SEBEHODNOCENÍ:
#funkce funguje z velké části v pořádku, jen u delších hracích polí -
# - jmenovitě 2OO vrací o 3 jinou hodnotu než byla uvedena v zadání
def game_average_turns(length, repetitions = 1000, prob_six = 1/6, output = True):
    sum_of_turns = 0
    for i in range(repetitions):
        sum_of_turns += game(length, prob_six=prob_six, output=False)
    if output == True:
        print ("Game of length ", length, " takes ", round(sum_of_turns/repetitions), " turns in average.")
    return sum_of_turns/repetitions

# SEBEHODNOCENÍ:
# funkce funguje jak má, nenašel jsem v funkcionalitě žádný problém
def game_prob_analysis(length):
    best_result = [0,length*6]
    for i in range(0, 100, 5):
        new_prob_six = round(i*0.01, 2)
        avg_turns = game_average_turns(length, prob_six = new_prob_six, output = False)
        if best_result[1] > avg_turns:
            best_result = [new_prob_six, avg_turns]
        print ("Prob. of six: ", str(new_prob_six).ljust(5), "   Average turns: ", avg_turns)
    print ("Best setting for length", length, ": ", best_result[0])
    return best_result[0]
 
