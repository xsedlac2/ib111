# Autor: Martin Sedláček, 485091

#Prohlašuji, že celý zdrojový kód jsem zpracoval(a) zcela samostatně.
#Jsem si vědom(a), že  nepravdivost tohoto tvrzení může být důvodem
#k hodnocení F v předmětu IB111 a k disciplinárnímu řízení.

from turtle import Turtle
zaneta = Turtle()


# SEBEHODNOCENÍ:
# kód funguje správně, a pro všechny n, kde n>0
# trošku krkolomně je řešená podmínka, pro psaní/nepsaní čárky za prvkem
def alternating_sequence (n):
    for i in range (1,n+1):
        if i%2==0:
            print (-i, end="")
        else:
            print (i, end="")
        if i != n:
            print(",", end=" ")

# SEBEHODNOCENÍ:
# kód funguje jak má, tabulku vypisuje korektně
# nevhodné je řešení pomocí mnoha podmínek, které zajišťuje správné vypisování popisných řádků a sloupců
# bohužel ale nedokážu vymyslet lepší řešení, jak popisné řádky a sloupce ošetřit
def divisible_table(n):
    for row in range(0,n+2):
        for column in range(0,n+2):
            if row == 0:
                if column < 2:
                    print (str(" ").ljust(2), end=" ")
                if column >= 2:
                    print (str(column - 1).ljust(2), end=" ")
            if  row == 1:
                if column < 2:
                    print (str(" ").ljust(2), end=" ")
                if column >= 2:
                    print (str("-").ljust(2), end=" ")
            if row >= 2:
                if column == 0:
                    print (str(row-1).ljust(2), end=" ")
                if column == 1:
                    print (str("|").ljust(2), end=" ")
                if column >= 2:
                    if (row - 1)%(column - 1) == 0:
                        print (str("T").ljust(2), end=" ")
                    else:
                        print (str("F").ljust(2), end=" ")
        print()

        


# SEBEHODNOCENÍ:
# kód funguje jak má, přesípací hodiny se vykreslují správně ve včech případech
def hourglass(size):
    num_of_hashes = size
    num_of_spaces = 0
    center = size//2
    num_of_repets = size+1-size%2
    for i in range (num_of_repets):
        if num_of_hashes != 0:
            print(" "*num_of_spaces, end="")
            print ("#"*num_of_hashes)
        if i < center:
            num_of_hashes -= 2
            num_of_spaces +=1
        else:
            num_of_hashes += 2
            num_of_spaces -=1

        

# SEBEHODNOCENÍ:
# kód vypisuje "M" korektně pro všechny případy
# kód je možná psán až na mnoho úrovní a je příliš větvený
def draw_m(n):
    tip_of_m = (n//2)-1+(n%2)
    spacebar = 0
    for i in range (n):
        if i>0 and i<=tip_of_m:
            for j in range (1,-1,-1):
                if i!=tip_of_m or j!=0 or n%2!=1:
                    print ("#", end="")
                print (" "*spacebar, end="")
                print ("#", end="")
                print (" "*((n-spacebar*2-4)*j), end="")
            spacebar +=1
            print ()
        else:
            print ("#", end="")
            print (" "*(n-2), end="")
            print ("#")

# SEBEHODNOCENÍ:
# kód funguje jak má
# jediný problém vidím v tom, že se rozhraní pro želví grafiku otevírá již při načtení skriptu a ne ža po zavolání funkce - tento problém má i úloha č.6
def honeycomb():
    zaneta.reset()
    size = 50
    polygon_6(size, "r")
    for j in range (6):
        polygon_6(size, "l")
        zaneta.forward(size)
        zaneta.right(60)

def polygon_6(size, turn):
    for i in range (6):
        zaneta.forward(size)
        if turn == "r":
            zaneta.right(60)
        else:
            zaneta.left(60)

# SEBEHODNOCENÍ:
# kód nefunguje korektně pro některé případy - pro čísla kdy platí, že petals//3==0
def flower(petals):
    zaneta.reset()
    for j in range (petals):
        for i in range (10):
            zaneta.forward(16)
            zaneta.right(720/petals/10)
        zaneta.right(360/petals)
