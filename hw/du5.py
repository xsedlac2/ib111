"""
Autor: MARTIN SEDLÁČEK, 485091

Prohlašuji, že celý zdrojový kód jsem zpracoval(a) zcela samostatně.
Jsem si vědom(a), že  nepravdivost tohoto tvrzení může být
důvodem k hodnocení F v předmětu IB111 a k disciplinárnímu řízení.

SEBEHODNOCENÍ A STYL:
Se zpracování úkolu 2 a 3 jsem spokojený, jsou funkční a stylisticky napsány správně.
Úkol č.1 je funkční, obrázek jedna je vykreslován podle zadání, ale druhý obrázek
vykresluje něco jiného než v zadání, protože předlohy jsem nebyl schopný dosáhnout.
Styl v úkolu dva také není nejlepší, někdy se opakují části kódu a možná by
bylo vhodné některé funkce rozložit na funkce menší.

"""
import math
import operator
from PIL import Image
from collections import defaultdict


class Turtle:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
        self.heading = 0  # in degrees
        self.lines = []  # list of tuple (x1, y1, x2, y2)

    def left(self, angle):
        self.heading -= angle

    def right(self, angle):
        self.heading += angle

    def forward(self, d):
        nx = self.x + d * math.cos(self.heading * math.pi / 180)
        ny = self.y + d * math.sin(self.heading * math.pi / 180)
        self.lines.append((self.x, self.y, nx, ny))
        self.x, self.y = nx, ny

    def turn_to_turtle(self, turtle):
        """
        Otoci zelvu tak, aby smerovala k zadane zelve.

        :param turtle: zelva (instance Turtle), ke ktere se otocit
        """
        distance_x = abs(self.x - turtle.x)
        distance_y = abs(self.y - turtle.y)
        if distance_x == 0 or distance_y == 0:
            new_angle = 0
        else:
            new_angle = math.atan(distance_x / distance_y) / math.pi * 180
        if self.x < turtle.x and self.y < turtle.y:
            self.heading = 90 - new_angle#
        if self.x > turtle.x and self.y < turtle.y:
            self.heading = new_angle + 90#
        if self.x < turtle.x and self.y > turtle.y:
            self.heading = new_angle + 270#
        if self.x > turtle.x and self.y > turtle.y:
            self.heading = 270 - new_angle #
            
    def connect_turtles(self, turtle):
        """
        Vykresli spojnici mezi sebou a zadanou zelvou. Ani jedna z zelev se
        neposune.

        :param turtle: zelva (instance Turtle), ke ktere vykreslit spojnici
        """
        self.lines.append((self.x, self.y, turtle.x, turtle.y))

def save (filename, t1, turtles):
    OFFSET = 200
    f = open(filename, "w")
    f.write('<svg xmlns="http://www.w3.org/2000/svg">')
    s = '<line x1="{}" y1="{}" x2="{}" y2="{}" style="{}"/>'
    for x1, y1, x2, y2 in t1.lines:
        f.write(s.format(x1+OFFSET, y1+OFFSET, x2+OFFSET, y2+OFFSET,"stroke:black;stroke-width:1"))
    for turtle in turtles:
        for x1, y1, x2, y2 in turtle.lines:
            f.write(s.format(x1+OFFSET, y1+OFFSET, x2+OFFSET, y2+OFFSET,"stroke:black;stroke-width:1"))
    f.write("</svg>")
    f.close()

def draw_svg_image1(filename):
    """
    Funkce, ktera vykresli nejaky pekny netrivialni obrazek pomoci objektove
    zelvi grafiky a ulozi jej do souboru "image1.svg" ve formatu SVG.
    """
    t = Turtle(-50,0)
    turtles = []
    
    for i in range(12):
        turtles.append("t"+str(i))
        
    for angle in range (1,13):
        turtles[angle-1] = Turtle(math.cos(angle*(2/12)*math.pi)*50, math.sin(angle*(2/12)*math.pi)*50)
        
    for j in range(100):
        t.forward(1)
        for turtle in turtles:
            turtle.turn_to_turtle(t)
            turtle.forward(1)
    save(filename, t, turtles)
    

def draw_svg_image2(filename):
    """
    Funkce, ktera vykresli nejaky pekny netrivialni obrazek pomoci objektove
    zelvi grafiky a ulozi jej do souboru "image2.svg" ve formatu SVG.
    """
    t01 = Turtle(0,0)
    t02 = Turtle(0,0)
    
    for i in range(180):
        t01.left(4)
        t01.forward(4)
        t02.left(10)
        t02.forward(10)
        t01.connect_turtles(t02)
        
    save(filename, t01, [t02])
        

draw_svg_image1("t2.svg")
draw_svg_image2("t0.svg")


def median_filter(image_path):
    """
    Funkce aplikuje na zadany obrazek median filter velikosti 3x3 a vysledek
    ulozi do stejne slozky pod nazvem souboru XXX_filtered.YYY, kde XXX.YYY
    je nazev originalniho souboru. 

    Median filter je operace, ktera pro kazdy pixel vezme jeho hodnotu a
    hodnoty okolnich osmi pixelu (celkem devet cisel) a priradi mu hodnotu,
    ktera je medianem teto devitice, tzn. patou nejvetsi hodnotu. 

    Filter lze testovat na obrazku "cameraman.png", ocekavany vystup je
    zobrazeny na obrazku "cameraman_filtered.png"

    :param image_path: string obsahujici cestu k obrazku
    """
    image_name = image_path.split(".")
    new_name = image_name[0] + "_filtered." + image_name[1]
    img = Image.open(image_path)
    img = img.convert("L")
    width, height = img.size
    img_new = Image.new("L", (width,height))
    for x in range(width):
        for y in range(height):
            colors = []
            for x_offset in range(-1,2):
                for y_offset in range (-1,2):
                    if 0 <= x+x_offset < width and  0 <= y+y_offset < height:
                        colors.append(img.getpixel((x+x_offset, y+y_offset)))
            img_new.putpixel((x,y),median(colors))
    img_new.save(new_name)
    return ("Done!")

def median(alist):
    alist.sort()
    center = len(alist)//2
    if len(alist) % 2 == 1:
        return alist[center]
    else:
        return int(sum(alist[center-1:center+1])/2)
        


def rhymes_with(word, file_path):
    """
    Funkce najde 5 nejlepe se rymujicich slov se slovem v parametru "word"
    v souboru slov nachazejicim se na ceste "file_path".
    :param word: string se slovem
    :param file_path: string s cestou k souboru se slovy, soubor obsahuje jedno
                      slovo na jeden radek
    :return: seznam 5 nejlepe se rymujicich slov se zadanym slovem

    >>> print(rhymes_with("kuskus", "ceska_slova.txt"))
    ['meniskus', 'skus', 'abakus', 'autofokus', 'cirkus']

    >>> print(rhymes_with("sedmikráska", "ceska_slova.txt"))
    ['sedmikráska', 'kráska', 'jiráska', 'teráska', 'vráska']
    """
    rhymes = defaultdict(list)
    ending_letters = 1
    with open(file_path, "r") as file:
            
            for line in file.readlines():
                line = line.strip()
                num_of_same_latters = -1
                
                for i in range(1,min(len(word), len(line))+1):
                    breaked = False
                    num_of_same_latters += 1                    
                    if word[-i] != line[-i]:
                        if num_of_same_latters >= ending_letters:
                            rhymes[num_of_same_latters].append(line)
                        breaked = True
                        break
                    
                if breaked == False and num_of_same_latters+1 >= ending_letters:
                        rhymes[num_of_same_latters+1].append(line)
                        
                if ending_letters in rhymes.keys():
                    if len(list(rhymes.get(ending_letters))) >= 5:
                        ending_letters +=1

    return get_best_results(rhymes, 5)

def get_best_results(rhymes, n):
    max_same_letters = max(list(rhymes.keys()))
    result_list = []
    
    while len(result_list) < n:
        if max_same_letters in rhymes.keys():
            result_list += rhymes.pop(max_same_letters)
        max_same_letters -= 1
        
    return result_list[0:n]
    
        
