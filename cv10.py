import math

class Rectangle():
    def __init__(self, width, height):
        self.width = width
        self.height = height

def area(rectangle):
    return rectangle.width * rectangle.height

class Circle():
    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r

    def perimeter(self):
        return 2 * math.pi * self.r

    def area(self):
        return math.pi * self.r * self.r

class Stack():
    def __init__(self):
        self.array = []

    def push(self, num):
        self.array.append(num)

    def pop(self):
        if not self.is_empty():
            print (self.array[len(self.array)-1])
            del self.array[len(self.array)-1]

    def is_empty(self):
        return len(self.array) == 0
