from collections import defaultdict
def most_common_names(file_name):

    frequencies = {}
    
    with open(file_name, "r") as file:
        
        for line in file.readlines():
            line = line.strip()
            line = line.split(", ")
            name = line[0]
            number = int(line[1])
            frequencies[name] = number

    sorted_names = sorted(frequencies.keys(), key=lambda name:frequencies[name], reverse=True)
    return sorted_names[:5]

def load_names(file_name):

    names_15 = []
    
    with open(file_name, "r") as file:
        
        for line in file.readlines():
            line = line.strip()
            line = line.split(", ")
            if int(line[1]) < 15 :
                names_15.append(line[0])

    return sorted(names_15)

def load_files(file_name):
    
    files = {}

    with open(file_name, "r") as file:

        for line in file.readlines():
            line = line.strip()
            if line[0:2] == "-r":
                file = line.split(" ")[2].split(".")
                files[file[1]] = file[0]
    return files

def load_files1(file_name):
    
    files = defaultdict(list)

    with open(file_name, "r") as file:

        for line in file.readlines():
            line = line.strip()
            if line[0:2] == "-r":
                file = line.split(" ")[2].rsplit(".", 1)
                files[file[1]].append(str(file[0]))
    return files

def load_fairy_tales(file_name):

    fairy_tales = {}

    with open(file_name, "r") as file:

        text = ""
        count = -1
        
        for line in file.readlines():
            line = line.strip()
            if len(line) != 0 and line[0] == "#":
                if count >= 0:
                    fairy_tales[name] = text
                    text = ""
                name = line[1:]
                count += 1 
            text += line + " "
        fairy_tales[name] = text
            
    return fairy_tales




            


    
