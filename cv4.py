from random import random, randint

def dice ():
    return randint(1,6)

def turn():
    sum = 0
    this_turn_num = dice()
    while this_turn_num%2 == 0:
        sum += this_turn_num
        print ("#", end=" ")
        print(this_turn_num)
        this_turn_num = dice()
    return sum

def coin_flip():
    return randint(1,100)<=85
    
