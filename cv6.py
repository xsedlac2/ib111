from random import randint, random
import math

def guess_number_human(upper_bound):
    num = randint(0, upper_bound)
    user = -1
    while user != num:
        user = int(input("Zadej svůj tip: "))
        if num < user:
            print("Moje číslo je menší.")
        elif num > user:
            print("Moje číslo je větší.")
        else:
            print("Jo, to je ono!")

def guess_number_pc(upper_bound):
    up = upper_bound
    down = 0
    while True:
        num = (up+down)//2
        string = str("Je cislo " + str(num) + " mensi (0), rovno (1), nebo vetsi (2) nez tvoje cislo?")
        user = int(input(string))
        if user == 0:
            up = num-1
        elif user == 2:
            down = num+2
        elif user == 1:
            print("Tvoje číslo je ", num, end="")
            print()
            return num

def binary_search(needle, haystack):
    up = len(haystack)
    down = 0
    for _ in range(math.ceil(math.log(len(haystack)+1,2))):
        mid = (up+down)//2
        #print(down, up, mid)
        if haystack[mid] < needle:
            down = mid
        elif haystack[mid] > needle:
            up = mid
        elif haystack[mid] == needle:
            return True
    return False

def binary_search_position(needle, haystack):
    up = len(haystack)
    down = 0
    for _ in range(math.ceil(math.log(len(haystack)+1,2))):
        mid = (up+down)//2
        #print(down, up, mid)
        if haystack[mid] < needle:
            down = mid
        elif haystack[mid] > needle:
            up = mid
        elif haystack[mid] == needle:
            return mid
    return -1

def binary_search_position_list(needle, haystack):
    pass



            
        
